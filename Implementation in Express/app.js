const express = require("express");
const { sequelize, User } = require("./models");

const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.set("view engine", "ejs");

// Get all users
app.get("/users", (req, res) => {
  User.findAll().then((users) => {
    res.render("users/index", {
      users,
    });
  });
});

// Form input a user
app.get("/users/create", (req, res) => {
  res.render("users/create");
});

// Create a new user
app.post("/users", (req, res) => {
  User.create({
    name: req.body.name,
    email: req.body.email,
    role: req.body.role,
  }).then((users) => {
    res.send("User berhasil dibuat");
  });
});

// Delete an user
app.get("/delete/(:uuid)", (req, res) => {
  User.destroy({
    where: { uuid: req.params.uuid },
  }).then((users) => {
    res.render("users/delete");
  });
});

// Form update an user
app.get("/update/(:uuid)", (req, res) => {
  User.findOne({
    where: { uuid: req.params.uuid },
  }).then((users) => {
    res.render("users/update", {
      uuid: users.uuid,
      name: users.name,
      email: users.email,
      role: users.role,
    });
  });
});

// Update user by uuid
app.post("/users/update/(:uuid)", (req, res) => {
  User.update(
    {
      name: req.body.name,
      email: req.body.email,
      role: req.body.role,
    },
    {
      where: { uuid: req.params.uuid },
    }
  )
    .then((users) => {
      res.send("User berhasil diupdate");
    })
    .catch((err) => {
      res.status(500).json("Can't update an user");
    });
});

// Server listen on port 3000
app.listen({ port: 3000 }, async () => {
  console.log("Server up on htpp://localhost:3000");
  await sequelize.authenticate();

  console.log("Database Connected!");
});
