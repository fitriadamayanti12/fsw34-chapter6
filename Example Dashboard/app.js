const StorageCtrl = (function () {
  return {
    paketKursus: function (item) {
      let items;

      if (localStorage.getItem("items") === null) {
        items = [];

        items.push(item);

        localStorage.setItem("items", JSON.stringify(items));
      } else {
        items = JSON.parse(localStorage.getItem("items"));

        items.push(item);

        localStorage.setItem("items", JSON.stringify(items));
      }
    },

    getItemsFromStorage: function () {
      let items;

      if (localStorage.getItem("items") === null) {
        items = [];
      } else {
        items = JSON.parse(localStorage.getItem("items"));
      }
      return items;
    },
    updateItemStorage: function (updatedItem) {
      let items = JSON.parse(localStorage.getItem("items"));

      items.forEach(function (item, index) {
        if (updatedItem.id === item.id) {
          items.splice(index, 1, updatedItem);
        }
      });

      localStorage.setItem("items", JSON.stringify(items));
    },
    deleteItemFromStorage: function (id) {
      let items = JSON.parse(localStorage.getItem("items"));

      items.forEach(function (item, index) {
        if (id === item.id) {
          items.splice(index, 1);
        }
      });
      localStorage.setItem("items", JSON.stringify(items));
    },
    clearItemFromStorage: function () {
      localStorage.removeItem("items");
    },
  };
})();

const ItemCtrl = (function () {
  const Item = function (id, nama, harga) {
    this.id = id;
    this.nama = nama;
    this.harga = harga;
  };

  const data = {
    items: StorageCtrl.getItemsFromStorage(),

    currentItem: null,
    totalHarga: 0,
  };

  return {
    getItems: function () {
      return data.items;
    },
    addItem: function (nama, harga) {
      let ID;

      if (data.items.length > 0) {
        ID = data.items[data.items.length - 1].id + 1;
      } else {
        ID = 0;
      }

      harga = parseInt(harga);

      newItem = new Item(ID, nama, harga);

      data.items.push(newItem);

      return newItem;
    },
    getItemById: function (id) {
      //untuk buat id
      let found = null;

      data.items.forEach(function (item) {
        if (item.id === id) {
          found = item;
        }
      });
      return found;
    },
    updateItem: function (nama, harga) {
      harga = parseInt(harga);

      let found = null;

      data.items.forEach(function (item) {
        if (item.id === data.currentItem.id) {
          item.nama = nama;
          item.harga = harga;
          found = item;
        }
      });
      return found;
    },
    deleteItem: function (id) {
      //get id
      const ids = data.items.map(function (item) {
        return item.id;
      });

      const index = ids.indexOf(id);

      data.items.splice(index, 1);
    },
    clearAllItem: function () {
      data.items = [];
    },
    setCurrentItem: function (item) {
      data.currentItem = item;
    },
    getCurrentItem: function () {
      return data.currentItem;
    },
    getTotalHarga: function () {
      let total = 0;

      //Looping item dan tambah kelas
      data.items.forEach(function (item) {
        total += item.harga;
      });

      //Setting total data
      data.totalHarga = total;

      //Return total
      return data.totalHarga;
    },
    logData: function () {
      return data;
    },
  };
})();

const UICtrl = (function () {
  const UISelector = {
    itemList: "#item-list",
    addBtn: ".add-btn",
    listItems: "#item-list li",
    updateBtn: ".update-btn",
    deleteBtn: ".delete-btn",
    clearBtn: ".clear-btn",
    backBtn: ".back-btn",
    itemNamaPaket: "#nama-paket",
    itemHargaPaket: "#harga-paket",
    totalHarga: ".total-harga",
  };

  return {
    populateItemList: function (items) {
      let html = "";
      items.forEach(function (item) {
        html += `<li class="collection-item" id="item-${item.id}">
                <strong>${item.nama}: </strong><em>Rp. ${item.harga}</em>
                <a href="#" class="secondary-content">
                    <i class="edit-item fa fa-pencil"></i>
                </a>
            </li>`;
      });

      document.querySelector(UISelector.itemList).innerHTML = html;
    },
    getItemInput: function () {
      return {
        nama: document.querySelector(UISelector.itemNamaPaket).value,
        harga: document.querySelector(UISelector.itemHargaPaket).value,
      };
    },
    addListItem: function (item) {
      document.querySelector(UISelector.itemList).style.display = "block";

      const li = document.createElement("li");

      li.className = "collection-item";

      li.id = `item-${item.id}`;

      li.innerHTML = `<strong>${item.nama}: </strong><em>${item.harga}</em>
            <a href="#" class="secondary-content">
                <i class="edit-item fa fa-pencil"></i>
            </a>`;

      document
        .querySelector(UISelector.itemList)
        .insertAdjacentElement("beforeend", li);
    },
    updateListItem: function (item) {
      let listItems = document.querySelectorAll(UISelector.listItems);

      listItems = Array.from(listItems);
      listItems.forEach(function (listItem) {
        const itemID = listItem.getAttribute("id");

        if (itemID === `item-${item.id}`) {
          document.querySelector(
            `#${itemID}`
          ).innerHTML = `<li class="collection-item" id="item-${item.id}">
                    <strong>${item.nama}: </strong><em>Rp. ${item.harga}</em>
                    <a href="#" class="secondary-content">
                        <i class="edit-item fa fa-pencil"></i>
                    </a>
                </li>`;
        }
      });
    },
    deleteListItem: function (id) {
      const itemID = `#item-${id}`;

      const item = document.querySelector(itemID);

      item.remove();
    },
    clearInput: function () {
      document.querySelector(UISelector.itemNamaPaket).value = "";
      document.querySelector(UISelector.itemHargaPaket).value = "";
    },
    addItemToForm: function () {
      document.querySelector(UISelector.itemNamaPaket).value =
        ItemCtrl.getCurrentItem().nama;
      document.querySelector(UISelector.itemHargaPaket).value =
        ItemCtrl.getCurrentItem().harga;

      UICtrl.showEditState();
    },
    removeItems: function () {
      let listItems = document.querySelectorAll(UISelector.listItems);

      listItems = Array.from(listItems);

      listItems.forEach(function (item) {
        item.remove();
      });
    },
    hideList: function () {
      document.querySelector(UISelector.itemList).style.display = "none";
    },
    showTotalHarga: function (totalHarga) {
      document.querySelector(UISelector.totalHarga).textContent = totalHarga;
    },
    clearEditState: function () {
      UICtrl.clearInput();
      document.querySelector(UISelector.updateBtn).style.display = "none";
      document.querySelector(UISelector.deleteBtn).style.display = "none";
      document.querySelector(UISelector.backBtn).style.display = "none";
      document.querySelector(UISelector.addBtn).style.display = "inline";
    },
    showEditState: function () {
      document.querySelector(UISelector.updateBtn).style.display = "inline";
      document.querySelector(UISelector.deleteBtn).style.display = "inline";
      document.querySelector(UISelector.backBtn).style.display = "inline";
      document.querySelector(UISelector.addBtn).style.display = "none";
    },
    getSelectors: function () {
      return UISelector;
    },
  };
})();

const App = (function (ItemCtrl, StorageCtrl, UICtrl) {
  const loadEventListeners = function () {
    const UISelector = UICtrl.getSelectors();
    //Simpan data
    document
      .querySelector(UISelector.addBtn)
      .addEventListener("click", itemAddSubmit);
    //Agar enter tidak berfungsi
    document.addEventListener("keypress", function (e) {
      if (e.keyCode === 13 || e.which === 13) {
        e.preventDefault;
        return false;
      }
    });
    //Edit click data to form
    document
      .querySelector(UISelector.itemList)
      .addEventListener("click", itemEditClick);
    //untuk update
    document
      .querySelector(UISelector.updateBtn)
      .addEventListener("click", itemUpdateSubmit);

    //delete button
    document
      .querySelector(UISelector.deleteBtn)
      .addEventListener("click", itemDeleteSubmit);

    //back button event
    document
      .querySelector(UISelector.backBtn)
      .addEventListener("click", UICtrl.clearEditState);

    //Hapus semua data
    document
      .querySelector(UISelector.clearBtn)
      .addEventListener("click", clearAllItemClick);
  };

  const itemAddSubmit = function (e) {
    //console.log('Add');

    const input = UICtrl.getItemInput();
    //console.log(input);
    if (input.nama !== "" && input.harga !== "") {
      const newItem = ItemCtrl.addItem(input.nama, input.harga);
      UICtrl.addListItem(newItem);

      const totalHarga = ItemCtrl.getTotalHarga();

      //Menampilkan total harga to UI
      UICtrl.showTotalHarga(totalHarga);

      StorageCtrl.paketKursus(newItem);

      UICtrl.clearInput();
    }
    e.preventDefault();
  };

  const itemEditClick = function (e) {
    if (e.target.classList.contains("edit-item")) {
      //Mengambil l;ist item berdasarkan ID
      const listId = e.target.parentNode.parentNode.id;
      //Memasukkan ke dalam sebuah array
      const listIdArr = listId.split("-");
      //Ambil Id yang sebenarnya
      const id = parseInt(listIdArr[1]);
      //Ambil item
      const itemToEdit = ItemCtrl.getItemById(id);

      ItemCtrl.setCurrentItem(itemToEdit);

      UICtrl.addItemToForm();
    }
    e.preventDefault();
  };

  const itemUpdateSubmit = function (e) {
    //Mengambil nilai inputan
    const input = UICtrl.getItemInput();

    const updatedItem = ItemCtrl.updateItem(input.nama, input.harga);

    UICtrl.updateListItem(updatedItem);

    const totalHarga = ItemCtrl.getTotalHarga();

    UICtrl.showTotalHarga(totalHarga);

    StorageCtrl.updateItemStorage(updatedItem);

    UICtrl.clearEditState();

    e.preventDefault();
  };

  const itemDeleteSubmit = function (e) {
    //Untuk mengambil item yang akan dihapus
    const currentItem = ItemCtrl.getCurrentItem();
    //Untuk struktur data berdasarkan id
    ItemCtrl.deleteItem(currentItem.id);
    //Menghapus form di UI
    UICtrl.deleteListItem(currentItem.id);

    const totalHarga = ItemCtrl.getTotalHarga();

    UICtrl.showTotalHarga(totalHarga);

    StorageCtrl.deleteItemFromStorage(currentItem.id);

    UICtrl.clearEditState();

    e.preventDefault();
  };

  const clearAllItemClick = function () {
    //Untuk hapus semua data di form/table
    ItemCtrl.clearAllItem();

    const totalHarga = ItemCtrl.getTotalHarga();

    UICtrl.showTotalHarga(totalHarga);

    UICtrl.removeItems();
    StorageCtrl.clearItemFromStorage();
    //Hide url
    UICtrl.hideList();
  };

  return {
    init: function () {
      UICtrl.clearEditState();

      const items = ItemCtrl.getItems();

      if (items.length === 0) {
        UICtrl.hideList();
      } else {
        UICtrl.populateItemList(items);
      }

      const totalHarga = ItemCtrl.getTotalHarga();

      //Menampilkan total harga to UI
      UICtrl.showTotalHarga(totalHarga);

      loadEventListeners();
    },
  };
})(ItemCtrl, StorageCtrl, UICtrl);

App.init();
