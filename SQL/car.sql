create table car (
	id BIGSERIAL NOT NULL PRIMARY KEY,
	make VARCHAR(100) NOT NULL,
	model VARCHAR(100) NOT NULL,
	price NUMERIC(19, 2) NOT NULL
);
insert into car (id, make, model, price) values (1, 'Pontiac', 'Parisienne', '52873.14');
insert into car (id, make, model, price) values (2, 'Nissan', 'Sentra', '65984.95');
insert into car (id, make, model, price) values (3, 'Dodge', 'Colt', '39152.15');
insert into car (id, make, model, price) values (4, 'Kia', 'Rio', '58557.18');
insert into car (id, make, model, price) values (5, 'Ford', 'Fusion', '84644.98');
insert into car (id, make, model, price) values (6, 'Dodge', 'Intrepid', '89660.61');
insert into car (id, make, model, price) values (7, 'Ford', 'Escort', '96991.06');
insert into car (id, make, model, price) values (8, 'Infiniti', 'FX', '99739.04');
insert into car (id, make, model, price) values (9, 'Volkswagen', 'Golf', '27326.26');
insert into car (id, make, model, price) values (10, 'Ford', 'Focus', '79917.22');
insert into car (id, make, model, price) values (11, 'Honda', 'CR-V', '74003.08');
insert into car (id, make, model, price) values (12, 'Aston Martin', 'V8 Vantage', '95492.98');
insert into car (id, make, model, price) values (13, 'Jaguar', 'XJ Series', '82744.99');
insert into car (id, make, model, price) values (14, 'Aston Martin', 'DB9 Volante', '75771.83');
insert into car (id, make, model, price) values (15, 'Chevrolet', 'Astro', '79442.57');
insert into car (id, make, model, price) values (16, 'Subaru', 'Impreza', '81595.62');
insert into car (id, make, model, price) values (17, 'Infiniti', 'FX', '71996.03');
insert into car (id, make, model, price) values (18, 'Mercedes-Benz', 'S-Class', '34670.86');
insert into car (id, make, model, price) values (19, 'Cadillac', 'DeVille', '14554.09');
insert into car (id, make, model, price) values (20, 'Volvo', '850', '72607.93');
insert into car (id, make, model, price) values (21, 'Mazda', 'Mazda5', '20408.28');
insert into car (id, make, model, price) values (22, 'Subaru', 'Legacy', '92100.12');
insert into car (id, make, model, price) values (23, 'Infiniti', 'EX', '66309.26');
insert into car (id, make, model, price) values (24, 'Mitsubishi', 'Mighty Max', '44891.33');
insert into car (id, make, model, price) values (25, 'Saab', '9-5', '85404.09');
insert into car (id, make, model, price) values (26, 'Chevrolet', 'Prizm', '66826.26');
insert into car (id, make, model, price) values (27, 'Lincoln', 'Town Car', '19568.92');
insert into car (id, make, model, price) values (28, 'Mazda', 'RX-7', '23912.33');
insert into car (id, make, model, price) values (29, 'GMC', 'Savana 2500', '71538.13');
insert into car (id, make, model, price) values (30, 'Chevrolet', 'Tahoe', '51373.39');
insert into car (id, make, model, price) values (31, 'Mitsubishi', 'Montero Sport', '90100.93');
insert into car (id, make, model, price) values (32, 'Plymouth', 'Colt', '71134.15');
insert into car (id, make, model, price) values (33, 'Toyota', 'MR2', '59395.56');
insert into car (id, make, model, price) values (34, 'Mercury', 'Cougar', '77448.14');
insert into car (id, make, model, price) values (35, 'Toyota', 'Tacoma', '30876.08');
insert into car (id, make, model, price) values (36, 'Mercedes-Benz', 'Sprinter', '67532.69');
insert into car (id, make, model, price) values (37, 'Lincoln', 'Continental', '56912.77');
insert into car (id, make, model, price) values (38, 'Ford', 'Aspire', '32797.40');
insert into car (id, make, model, price) values (39, 'GMC', '1500', '51930.42');
insert into car (id, make, model, price) values (40, 'Mitsubishi', 'Lancer', '95019.42');
insert into car (id, make, model, price) values (41, 'Mitsubishi', 'Pajero', '52114.82');
insert into car (id, make, model, price) values (42, 'Volvo', '940', '56839.77');
insert into car (id, make, model, price) values (43, 'Saturn', 'VUE', '67250.25');
insert into car (id, make, model, price) values (44, 'Acura', 'RL', '78460.54');
insert into car (id, make, model, price) values (45, 'Suzuki', 'Samurai', '72705.04');
insert into car (id, make, model, price) values (46, 'Porsche', '944', '27653.21');
insert into car (id, make, model, price) values (47, 'Chevrolet', '3500', '40021.25');
insert into car (id, make, model, price) values (48, 'Land Rover', 'Range Rover Sport', '22654.07');
insert into car (id, make, model, price) values (49, 'Volvo', 'V70', '33533.06');
insert into car (id, make, model, price) values (50, 'Mitsubishi', 'Galant', '63570.08');
