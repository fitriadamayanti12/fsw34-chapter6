 create table car (
	id BIGSERIAL NOT NULL PRIMARY KEY,
	make VARCHAR(100) NOT NULL,
	model VARCHAR(100) NOT NULL,
	price NUMERIC(19, 2) NOT NULL
);
 
 create table person (
	id BIGSERIAL NOT NULL PRIMARY KEY,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	email VARCHAR(100),
	gender VARCHAR(7) NOT NULL,
	date_of_birth DATE NOT NULL,
	country_of_birth VARCHAR(50) NOT NULL,
    car_id BIGINT REFERENCES car (id),
    UNIQUE(car_id)
);

insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Putnam', 'Trevillion', 'ptrevillion0@behance.net', 'Male', '2022-07-14', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Joshuah', 'Haselden', null, 'Male', '2022-09-10', 'Bahamas');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Lane', 'Tierney', 'ltierney2@ebay.co.uk', 'Male', '2022-08-16', 'Syria');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Yevette', 'Deering', 'ydeering3@example.com', 'Female', '2023-03-19', 'Indonesia');

insert into car (id, make, model, price) values (1, 'Pontiac', 'Parisienne', '52873.14');
insert into car (id, make, model, price) values (2, 'Dodge', 'Colt', '39152.15');
insert into car (id, make, model, price) values (3, 'Ford', 'Fusion', '84644.98');