Link mock data -> https://www.mockaroo.com/
Execute command from file
 Jalankan perintah berikut di terminal -> \i /Users/user/downloads/person.sql

create table person (
	id BIGSERIAL NOT NULL PRIMARY KEY,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	email VARCHAR(50),
	gender VARCHAR(7) NOT NULL,
	date_of_birth DATE NOT NULL,
	country_of_birth VARCHAR(50)
);
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Putnam', 'Trevillion', 'ptrevillion0@behance.net', 'Male', '2022-07-14', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Joshuah', 'Haselden', null, 'Male', '2022-09-10', 'Bahamas');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Lane', 'Tierney', 'ltierney2@ebay.co.uk', 'Male', '2022-08-16', 'Syria');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Yevette', 'Deering', 'ydeering3@example.com', 'Female', '2023-03-19', 'Indonesia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Brod', 'Redsall', 'bredsall4@ning.com', 'Male', '2022-09-21', 'Argentina');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Sydelle', 'Melloi', null, 'Female', '2022-08-22', 'Czech Republic');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Floyd', 'Poure', null, 'Male', '2023-06-11', 'Greece');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Cathlene', 'Richold', 'crichold7@surveymonkey.com', 'Female', '2022-11-28', 'United States');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Ferdy', 'Adrienne', 'fadrienne8@nbcnews.com', 'Male', '2022-09-07', 'Japan');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Burlie', 'Chicotti', null, 'Male', '2022-09-15', 'Mexico');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Maribeth', 'Rosenberger', 'mrosenbergera@ca.gov', 'Female', '2022-11-23', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Reece', 'Pfeiffer', null, 'Male', '2023-03-02', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Bevvy', 'Grant', 'bgrantc@illinois.edu', 'Female', '2022-12-01', 'Sweden');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Collen', 'O''Donoghue', 'codonoghued@washingtonpost.com', 'Female', '2023-02-24', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Haven', 'Guilfoyle', 'hguilfoylee@simplemachines.org', 'Male', '2023-06-07', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Tobi', 'Haywood', 'thaywoodf@shutterfly.com', 'Female', '2022-09-14', 'Myanmar');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Gerri', 'Crassweller', null, 'Female', '2022-08-13', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Ruthann', 'Cogzell', null, 'Female', '2023-05-20', 'Thailand');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Estel', 'Freke', 'efrekei@virginia.edu', 'Female', '2023-06-15', 'Nigeria');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Natty', 'Gething', 'ngethingj@pen.io', 'Female', '2023-05-14', 'Russia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Shelby', 'O''Donoghue', null, 'Female', '2023-03-27', 'Russia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Mamie', 'Penelli', 'mpenellil@uiuc.edu', 'Agender', '2023-02-23', 'Brazil');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Abba', 'Inkle', null, 'Male', '2022-08-10', 'Japan');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Twila', 'Ardling', null, 'Female', '2023-05-11', 'Saudi Arabia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Kerrill', 'Colcutt', 'kcolcutto@msu.edu', 'Genderqueer', '2023-05-06', 'Poland');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Marylinda', 'Probbin', null, 'Female', '2022-09-07', 'Bangladesh');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Hettie', 'Hast', 'hhastq@joomla.org', 'Female', '2022-12-01', 'Portugal');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Clayborn', 'Evershed', null, 'Male', '2022-09-04', 'Tunisia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Holli', 'Sainsbury', 'hsainsburys@lulu.com', 'Female', '2023-06-01', 'Madagascar');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Rice', 'Marshalleck', 'rmarshalleckt@rakuten.co.jp', 'Agender', '2022-10-20', 'Tanzania');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Chester', 'Rickerd', 'crickerdu@dailymail.co.uk', 'Genderqueer', '2023-04-11', 'Brazil');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Aubry', 'Brilon', null, 'Female', '2023-02-01', 'Philippines');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Sara', 'Pierro', 'spierrow@abc.net.au', 'Female', '2022-10-14', 'Indonesia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Wernher', 'Pollen', 'wpollenx@google.com', 'Genderfluid', '2022-12-05', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Vivienne', 'Hofton', 'vhoftony@jalbum.net', 'Female', '2023-03-21', 'Argentina');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Rose', 'Greenwood', 'rgreenwoodz@huffingtonpost.com', 'Female', '2022-09-13', 'Tanzania');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Dane', 'Danet', 'ddanet10@cam.ac.uk', 'Agender', '2022-07-29', 'Philippines');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Michale', 'Buchett', 'mbuchett11@youtube.com', 'Male', '2023-03-05', 'Peru');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Coletta', 'Biasetti', 'cbiasetti12@uiuc.edu', 'Non-binary', '2023-02-21', 'Russia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Dominica', 'Yurov', null, 'Female', '2023-01-19', 'Indonesia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Urbain', 'O''Towey', 'uotowey14@ameblo.jp', 'Male', '2022-09-03', 'Italy');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Emmalynne', 'de Almeida', 'edealmeida15@sfgate.com', 'Female', '2023-05-31', 'Lithuania');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Lewie', 'Pendered', null, 'Agender', '2023-06-24', 'Ivory Coast');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Morgun', 'Gertray', 'mgertray17@dailymotion.com', 'Male', '2022-11-27', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Nero', 'Dawney', null, 'Male', '2022-11-07', 'Palau');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Nicolis', 'Deavall', null, 'Male', '2023-01-25', 'Poland');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Wilone', 'Giacomasso', 'wgiacomasso1a@reverbnation.com', 'Female', '2023-03-03', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Sacha', 'Brittin', 'sbrittin1b@shop-pro.jp', 'Female', '2023-03-30', 'United States');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Goran', 'Hakonsson', 'ghakonsson1c@hubpages.com', 'Male', '2023-04-28', 'Albania');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Cahra', 'Mallindine', 'cmallindine1d@un.org', 'Female', '2023-04-25', 'Indonesia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Muhammad', 'Shearston', 'mshearston1e@odnoklassniki.ru', 'Male', '2022-12-07', 'Indonesia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Wilbert', 'Emptage', 'wemptage1f@google.es', 'Male', '2022-12-01', 'Czech Republic');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Joshia', 'Tewkesberrie', 'jtewkesberrie1g@cloudflare.com', 'Male', '2023-02-23', 'Philippines');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Eben', 'Barnby', null, 'Genderfluid', '2022-09-24', 'Philippines');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Chrysler', 'Boothby', null, 'Female', '2022-09-04', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Benedetta', 'McCuish', 'bmccuish1j@wikimedia.org', 'Female', '2022-11-27', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Mathe', 'McQuaide', 'mmcquaide1k@netlog.com', 'Male', '2023-02-17', 'East Timor');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Evangelia', 'Dmitrievski', 'edmitrievski1l@last.fm', 'Female', '2022-10-22', 'Denmark');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Trish', 'Stallibrass', 'tstallibrass1m@cmu.edu', 'Female', '2023-04-15', 'Sweden');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Frannie', 'Spiring', 'fspiring1n@auda.org.au', 'Male', '2023-02-26', 'Portugal');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Janice', 'Youngman', 'jyoungman1o@wordpress.com', 'Female', '2022-10-05', 'Indonesia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Rayner', 'Berr', 'rberr1p@msu.edu', 'Male', '2023-03-25', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Lester', 'Bolduc', null, 'Male', '2023-05-26', 'Greece');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Venita', 'de Tocqueville', 'vdetocqueville1r@cnbc.com', 'Female', '2023-04-04', 'Yemen');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Moise', 'Elkington', 'melkington1s@storify.com', 'Male', '2022-12-12', 'Honduras');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Viviene', 'Muller', null, 'Female', '2023-06-14', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Mame', 'Nosworthy', 'mnosworthy1u@domainmarket.com', 'Female', '2022-08-29', 'Indonesia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Flossi', 'McFall', null, 'Female', '2022-10-14', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Poul', 'Angell', null, 'Male', '2022-08-25', 'Russia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Robbie', 'Durker', null, 'Male', '2022-09-09', 'Canada');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Brooks', 'Esslement', 'besslement1y@arizona.edu', 'Female', '2022-10-03', 'Peru');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Robbie', 'Gentil', 'rgentil1z@elegantthemes.com', 'Male', '2022-12-28', 'Argentina');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Ignaz', 'Dinjes', 'idinjes20@craigslist.org', 'Male', '2022-09-27', 'Thailand');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Roshelle', 'Leigh', 'rleigh21@photobucket.com', 'Female', '2022-09-07', 'Spain');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Callie', 'Deddum', 'cdeddum22@nationalgeographic.com', 'Female', '2023-02-14', 'Burkina Faso');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Hayward', 'Perrelle', null, 'Male', '2023-07-01', 'Russia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Hobard', 'Clowley', null, 'Agender', '2023-05-15', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Carolan', 'Barkly', 'cbarkly25@topsy.com', 'Female', '2022-09-12', 'Liberia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Rurik', 'Antonellini', null, 'Male', '2022-10-23', 'Argentina');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Bee', 'Boorer', 'bboorer27@t.co', 'Female', '2022-08-02', 'United States');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Nadya', 'Tinner', null, 'Female', '2023-03-12', 'Portugal');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Marjie', 'Beeres', null, 'Female', '2023-03-13', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Jeramey', 'Forster', 'jforster2a@noaa.gov', 'Male', '2023-03-10', 'Paraguay');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Shannon', 'Goward', 'sgoward2b@google.it', 'Female', '2022-11-09', 'Canada');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Dill', 'Paulat', 'dpaulat2c@wikia.com', 'Male', '2022-12-17', 'Thailand');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Fianna', 'Newbery', 'fnewbery2d@ed.gov', 'Female', '2023-05-05', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Kerrill', 'Groven', null, 'Female', '2023-02-02', 'Portugal');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Harriot', 'Prandoni', null, 'Female', '2023-03-07', 'Indonesia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Bay', 'Petrashov', 'bpetrashov2g@techcrunch.com', 'Male', '2022-09-04', 'Poland');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Leelah', 'Tidswell', null, 'Female', '2023-04-03', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Archambault', 'Sanham', 'asanham2i@livejournal.com', 'Male', '2023-01-17', 'Russia');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Debora', 'Reimer', null, 'Female', '2022-10-11', 'Philippines');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Sibylla', 'McLevie', 'smclevie2k@marketwatch.com', 'Female', '2023-04-24', 'Philippines');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Earle', 'Risdall', 'erisdall2l@histats.com', 'Male', '2022-10-30', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Carlynn', 'Mougenel', 'cmougenel2m@theatlantic.com', 'Female', '2023-03-05', 'Tanzania');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Evey', 'Lynn', 'elynn2n@xrea.com', 'Polygender', '2022-12-28', 'Japan');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Carlynne', 'McNaught', null, 'Female', '2023-01-17', 'China');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Donal', 'Lorent', null, 'Male', '2022-08-26', 'Costa Rica');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Roberta', 'Hellings', 'rhellings2q@reddit.com', 'Female', '2022-07-13', 'Mexico');
insert into person (first_name, last_name, email, gender, date_of_birth, country_of_birth) values ('Moritz', 'Dunbabin', 'mdunbabin2r@ihg.com', 'Male', '2023-03-29', 'Japan');
