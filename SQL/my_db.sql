CREATE TABLE articles(id BIGSERIAL PRIMARY KEY, title VARCHAR(255) NOT NULL, author VARCHAR(30) NOT NULL, body TEXT NOT NULL, approved BOOLEAN NOT NULL DEFAULT FALSE);

/* Perintah untuk insert data */
INSERT INTO articles(title, author, body, approved) VALUES('Database', 'Sabrina', 'Mari belajar membuat database', TRUE);
INSERT INTO articles(title, author, body, approved) VALUES('SQL', 'John Doe', 'Mari belajar memahami SQL', TRUE);
INSERT INTO articles(title, author, body, approved) VALUES('ORM', 'Sabrina', 'Mari belajar menerapkan ORM', TRUE);
INSERT INTO articles(title, author, body, approved) VALUES('Express', 'Anna', 'Mari belajar Express.js', FALSE);

/* Perintah untuk menampilkan data */
SELECT * FROM table_name;
SELECT * FROM articles;

/* Perintah untuk menampilkan data dengan kondisi tertentu */
SELECT * FROM table_name WHERE conditions;
SELECT * FROM articles WHERE author = 'Sabrina';

/* Perintah untuk update */
UPDATE articles SET author = 'Morgan Housel' WHERE id = 3; // Table hanya satu dan tidak sama nama field-nya
UPDATE articles SET author = 'Morgan Housel' WHERE articles.id = 3; // Table banyak dan sama nama field-nya (konvensi ini lebih umum dipakai)

/* Perintah delete */
DELETE FROM articles WHERE articles.id = 3;
DELETE FROM articles WHERE article.author = 'John Doe';


/* Transaction Control Language => COMMIT*/
BEGIN TRANSACTION;
INSERT INTO articles(title, author, body, approved) VALUES('Express','Anna', 'Mari belajar Express.js', FALSE);
COMMIT;

/* Perintah menampilkan data (cek data baru yang telah diinsert)*/
SELECT * FROM articles;

/* Transaction Control Language => ROLLBACK*/
BEGIN TRANSACTION;
INSERT INTO articles(title, author, body, approved) VALUES('OOP','Morgan', 'Mari belajar OOP', FALSE);

SELECT * FROM articles; /* Sudah tampil data baru*/
ROLLBACK;






