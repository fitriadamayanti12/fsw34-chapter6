/* 
psql -h localhost -p 5432 -U postgres, press enter, input your psql password
Connect to database -> \c <database-name> 
Execute command from file -> /i <file-path> -> /Users/user/downloads/person.sql
*/

/* Read first name, last name */
SELECT first_name, last_name FROM person;

/* Order by 
SELECT * FROM table_name ORDER BY column_name ASC or DESC
*/
SELECT * FROM person ORDER BY country_of_birth;
SELECT * FROM person ORDER BY date_of_birth ASC;
SELECT * FROM person ORDER BY country_of_birth ASC;
SELECT * FROM person ORDER BY country_of_birth DESC;

/* 
DISTINCT
SELECT DICTICT colum_name FROM table_name ORDER BY column_name;
*/
SELECT DISTINCT country_of_birth FROM person ORDER BY country_of_birth;

/* Where Clauses, AND & OR */
SELECT * FROM person WHERE gender = 'Female';
SELECT * FROM person WHERE gender = 'Male' AND country_of_birth = 'Indonesia';
SELECT * FROM person WHERE gender = 'Male' AND (country_of_birth = 'Indonesia' OR country_of_birth = 'China');
SELECT * FROM person WHERE gender = 'Male' AND (country_of_birth = 'Indonesia' OR country_of_birth = 'China') AND last_name = 'Berr';

/* Limit, Offset, Fetch */
SELECT * FROM person OFFSET 6;
SELECT * FROM person OFFSET 6 FIRST 5 ROW ONLY;

/* Group by -> Count */
SELECT country_of_birth,  COUNT(*) FROM person GROUP BY country_of_birth ORDER BY country_of_birth;

/* Like & ILIKE */
SELECT * FROM person WHERE email LIKE '%.com';
SELECT * FROM person WHERE email LIKE '___________%';

/* Create new table -> car */
\i /Users/user/downloads/car.sql
SELECT * FROM car;

/* Aggregate -> MAX, MIN, AVERAGE, ROUND */
SELECT MAX(price) FROM car;
SELECT AVG(price) FROM car;
SELECT make, model, MAX(price) FROM car GROUP BY make, model; 

/* Adding Relation Between Tables & INNER JOIN 
Execute command in terminal \i /Users/user/downloads/person_car.sql
*/
SELECT * FROM person JOIN car ON person.car_id = car.id;
