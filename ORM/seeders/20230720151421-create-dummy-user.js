"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    await queryInterface.bulkInsert(
      "users",
      [
        {
          name: "John Doe",
          email: "john@email.com",
          role: "admin",
          uuid: "169cd74e-e72c-4dcf-b2e7-46d5b36b4d91",
          createdAt: "2023-07-19T07:27:43.155Z",
          updatedAt: "2023-07-19T08:49:52.097Z",
        },
        {
          name: "Jane Smith",
          email: "jane@email.com",
          role: "user",
          uuid: "169cd74e-e72c-4dcf-b2e7-46d5b46b4d93",
          createdAt: "2023-07-19T07:27:43.155Z",
          updatedAt: "2023-07-19T08:49:52.097Z",
        },
        {
          name: "Sabrina",
          email: "sabrina@email.com",
          role: "superadmin",
          uuid: "169cd74e-e72c-4dcf-b2e7-46d5b46b4d93",
          createdAt: "2023-07-19T07:27:43.155Z",
          updatedAt: "2023-07-19T08:49:52.097Z",
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
