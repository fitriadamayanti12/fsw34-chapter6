const { Article } = require("./models");

const query = {
  where: { id: 1 },
};

Article.update(
  {
    title: "Express",
    author: "Sabrina",
    category: "Programming",
    body: "Belajar Express",
    approved: false,
  },
  query
)
  .then(() => {
    console.log("Artikel berhasil diupdate");
    process.exit();
  })
  .catch((err) => {
    console.log("Gagal mengupdate artikel");
    console.log(err);
  });
