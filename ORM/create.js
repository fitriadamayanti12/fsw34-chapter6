const { Article } = require("./models");

Article.create({
  title: "Database",
  author: "John",
  category: "Programming",
  body: "Belajar Database",
  approved: true,
}).then((article) => {
  console.log(article);
});
