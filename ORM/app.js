// Import express & models
const express = require("express");
const { sequelize, User, Post } = require("./models");

// Instance express
const app = express();
app.use(express.json());

// Create users -> Method POST
app.post("/users", async (req, res) => {
  const { name, email, role } = req.body;

  try {
    const user = await User.create({ name, email, role });

    return res.json(user);
  } catch (err) {
    return res.status(500).json(err);
  }
});

// Read all
app.get("/users", async (req, res) => {
  try {
    const users = await User.findAll();

    return res.json(users);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Something went wrong!" });
  }
});

// Read by uuid
app.get("/users/:uuid", async (req, res) => {
  const uuid = req.params.uuid;
  try {
    const user = await User.findOne({
      where: { uuid },
    });

    return res.json(user);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Something went wrong!" });
  }
});

// Create for posts
app.post("/posts", async (req, res) => {
  const { userUuid, category, body } = req.body;

  try {
    const user = await User.findOne({ where: { uuid: userUuid } });

    const post = await Post.create({ category, body, userId: user.id });

    return res.json(post);
  } catch (err) {
    return res.status(500).json(err);
  }
});

// Delete for user
app.delete("/users/:uuid", async (req, res) => {
  const uuid = req.params.uuid;
  try {
    const user = await User.findOne({
      where: { uuid },
    });

    await user.destroy();

    return res.json({ message: "User deleted!" });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Something went wrong!" });
  }
});

// Update for user
app.put("/users/:uuid", async (req, res) => {
  const uuid = req.params.uuid;
  const { name, email, role } = req.body;
  try {
    const user = await User.findOne({
      where: { uuid },
    });

    user.name = name;
    user.email = email;
    user.role = role;

    await user.save();

    return res.json(user);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Something went wrong!" });
  }
});

// Read all for posts
app.get("/posts", async (req, res) => {
  try {
    const posts = await Post.findAll({
      include: "user",
    });

    return res.json(posts);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Something went wrong!" });
  }
});

// Delete for post
app.delete("/posts/:uuid", async (req, res) => {
  const uuid = req.params.uuid;
  try {
    const post = await Post.findOne({
      where: { uuid },
    });

    await post.destroy();

    return res.json({ message: "Post deleted!" });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Something went wrong!" });
  }
});

// Update for posts
app.put("/posts/:uuid", async (req, res) => {
  const uuid = req.params.uuid;
  const { userId, category, body } = req.body;
  try {
    const post = await Post.findOne({
      where: { uuid },
    });

    post.category = category;
    post.body = body;
    post.userId = userId;

    await post.save();

    return res.json(post);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Something went wrong!" });
  }
});

// Server listen on port 3000
app.listen({ port: 3000 }, async () => {
  console.log("Server up on htpp://localhost:3000");
  await sequelize.authenticate();

  console.log("Database Connected!");
});
